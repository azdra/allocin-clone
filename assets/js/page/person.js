import {getElement} from '../lib.js';
import cfg from '../../../config.js'; // defult import 

let params = new URL(document.location);
let id = params.searchParams.get('person');

if(!id) window.location = '../index.html';

let goback = document.getElementById('button-back');
goback.addEventListener('click', () => {
	window.location = '../index.html';
});
let goback_header = document.getElementById('button-back-header');
goback_header.addEventListener('click', () => {
	window.location = '../index.html';
});



fetch(`https://api.themoviedb.org/3/person/${id}?api_key=${cfg.apikey}&language=${cfg.lang}`)
	.then(j => j.json())
	.then(res => result(res));

const result = (res) => {
    let p = getElement(res);
    
    console.log(res)

    let movie_elm_img = document.getElementById('elm-img');
    movie_elm_img.setAttribute('src', p.poster_path ? `https://image.tmdb.org/t/p/w500/${p.poster_path}` : '../../img/unknown.png'); 
    
    let movieTitle = document.getElementById('elmTitle');
	movieTitle.innerHTML = `${p.title}`;
	
	let elmBirthday = document.getElementById('elmBirthday');
    if(p.deathday){
		elmBirthday.innerHTML = `Née le ${p.birthday.split('-')[2]}<span class=\'round\'>/</span>${p.birthday.split('-')[1]}<span class=\'round\'>/</span>${p.birthday.split('-')[0]} et est mort le ${p.deathday.split('-')[2]}<span class=\'round\'>/</span>${p.deathday.split('-')[1]}<span class=\'round\'>/</span>${p.deathday.split('-')[0]}`;
	} else {
		elmBirthday.innerHTML = `Née le ${p.birthday.split('-')[2]}<span class=\'round\'>/</span>${p.birthday.split('-')[1]}<span class=\'round\'>/</span>${p.birthday.split('-')[0]}`
	}


    /* 
    let moviedesc = document.getElementById('elmdesc');
	moviedesc.innerHTML = (p.biography == '' || !p.biography) ? 'Aucune description donnée' : p.biography; 
 */

/* 	let tv = document.getElementById('movie-container');
	tv.style.backgroundImage = `url(https://image.tmdb.org/t/p/original/${p.backdrop_path})`;
    
	let movie_elm_img = document.getElementById('elm-img');
	movie_elm_img.setAttribute('src', `https://image.tmdb.org/t/p/w500/${p.poster_path}`); 
    
	
    
	let release_date = document.getElementById('elmRelease');
	release_date.innerText = `(${p.release_date.split('-')[0]})`;
	release_date.classList.add('elmsubtitle');
    
	let moviesubtitle = document.getElementById('elmsubtitle');
	moviesubtitle.innerHTML = `${p.release_date.split('-')[2]}<span class=\'round\'>/</span>${p.release_date.split('-')[1]}<span class=\'round\'>/</span>${p.release_date.split('-')[0]} `;

	let g = '';
	for(let k in p.genres){
		if(k != p.genres.length-1){
			g += p.genres[k].name +'<span class=\'round\'>,</span> ';
		} else {
			g += p.genres[k].name;
		}
	}
	let moviegenre = document.getElementById('elmgenre');
	moviegenre.innerHTML = ' <span class=\'round\'>•</span> '+ g;
	moviegenre.classList.add('elmgenre');

	let episodetime = document.getElementById('episodetime');
	episodetime.innerHTML = `Duré d'épisode: <span class=\'round\'>~</span> ${p.episode_time} <span class=\'round\'>mins</span>`

	let moviedesc = document.getElementById('elmdesc');
	moviedesc.innerHTML = (p.overview == '' || !p.overview) ? 'Aucune description donnée' : p.overview; 

	let networkname = document.getElementById('networkname');
	networkname.innerHTML = p.networks[0].name;

	let networkposter = document.getElementById('networkposter');
	networkposter.setAttribute('src', `https://image.tmdb.org/t/p/w500/${p.networks[0].logo_path}`); 

	let watchnow = document.getElementById('watchnow');
	watchnow.setAttribute('href', p.homepage)

	document.title = `AlluCiné - ${p.title}`; // Titre du doc
	console.log(res); */
};
