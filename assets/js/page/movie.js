import {getElement, mtoh} from '../lib.js';
import cfg from '../../../config.js'; // defult import 

let params = new URL(document.location);
let id = params.searchParams.get('movieid');

if(!id) window.location = '../index.html';

let goback = document.getElementById('button-back');
goback.addEventListener('click', () => {
	window.location = '../index.html';
});
let goback_header = document.getElementById('button-back-header');
goback_header.addEventListener('click', () => {
	window.location = '../index.html';
});

/* let gobackheader = document.getElementById('button-back-header')
gobackheader.addEventListener('click', () => {
	window.location = '../index.html';
})
 */

fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${cfg.apikey}&language=${cfg.lang}`)
	.then(j => j.json())
	.then(res => result(res));

const result = (res) => {
	console.log(res);
	let p = getElement(res);

	let movie_container = document.getElementById('movie-container');
	movie_container.style.backgroundImage = `url(https://image.tmdb.org/t/p/original/${p.backdrop_path})`;

	let movie_elm_img = document.getElementById('elm-img');
	movie_elm_img.setAttribute('src', `https://image.tmdb.org/t/p/w500/${p.poster_path}`); 

	let movieTitle = document.getElementById('elmTitle');
	movieTitle.innerHTML = `${p.title}`;

	let release_date = document.getElementById('elmRelease');
	release_date.innerText = `(${p.release_date.split('-')[0]})`;
	release_date.classList.add('elmsubtitle');

	let tagline = document.getElementById('tagline');
	tagline.innerHTML = p.tagline;

	let moviesubtitle = document.getElementById('elmsubtitle');
	moviesubtitle.innerHTML = `${p.release_date.split('-')[2]}<span class=\'round\'>/</span>${p.release_date.split('-')[1]}<span class=\'round\'>/</span>${p.release_date.split('-')[0]} `;

	let g = '';
	for(let k in p.genres){
		if(k != p.genres.length-1){
			g += p.genres[k].name +'<span class=\'round\'>,</span> ';
		} else {
			g += p.genres[k].name;
		}
	}
	let moviegenre = document.getElementById('elmgenre');
	moviegenre.innerHTML = ' <span class=\'round\'>•</span> '+ g;
	moviegenre.classList.add('elmgenre');
    
	let movietime = document.getElementById('elmtime');
	movietime.innerHTML = ` <span class=\'round\'>•</span> ${mtoh(p.runtime).hours}h${mtoh(p.runtime).minutes}m (${p.runtime} <span class=\'round\'>min</span>)`;
	let moviedesc = document.getElementById('elmdesc');
	moviedesc.innerHTML = (p.overview == '' || !p.overview) ? 'Aucune description donnée' : p.overview; 
	/**
     * @ATTRIBUTION DES META
     */

	document.title = `AlluCiné - ${p.title}`; // Titre du doc
    
	/*
	Vibrant.from(`https://image.tmdb.org/t/p/w500/${p.poster_path}`).getPalette().then((palette) => {
		let header = document.getElementById('header-color');
		header.style.background = `rgba(${palette.Vibrant['_rgb'][0]}, ${palette.Vibrant['_rgb'][1]}, ${palette.Vibrant['_rgb'][2]})`;
	}); // Couleur du theme*/

	document.querySelector('meta[name="title"]').setAttribute('content', `AlluCiné - ${p.title}`); // Titre
	document.querySelector('meta[name="description"]').setAttribute('content', `AlluCiné: ${p.overview}`); // description

	document.querySelector('meta[property="og:url"]').setAttribute('content', params.href); // og URL
	document.querySelector('meta[property="og:title"]').setAttribute('content', `AlluCiné - ${p.title}`); // og Title
	document.querySelector('meta[property="og:description"]').setAttribute('content', `AlluCiné: ${p.overview}`); // og Description
	document.querySelector('meta[property="og:image"]').setAttribute('content', `https://image.tmdb.org/t/p/w500/${p.poster_path}`); // og Poster
    
	document.querySelector('meta[property="twitter:card"]').setAttribute('content', `https://image.tmdb.org/t/p/w500/${p.poster_path}`); // twitter card
	document.querySelector('meta[property="twitter:url"]').setAttribute('content', params.href); // twitter url
	document.querySelector('meta[property="twitter:title"]').setAttribute('content', `AlluCiné - ${p.title}`); // twitter title
	document.querySelector('meta[property="twitter:description"]').setAttribute('content', `AlluCiné: ${p.overview}`); // twitter description
	document.querySelector('meta[property="twitter:image"]').setAttribute('content', `https://image.tmdb.org/t/p/w500/${p.poster_path}`); // twitter image

    
	/**
     * @ATTRIBUTION DES META
     */
}; 


/* let loading = new Promise( (r, e) => {
	console.log('Recherche de la solution...')
	setInterval(() => {
		r()
	}, (60*1000)*60);
})

loading.then(r => {
	console.log('Aucune solution trouvé!');
	altf4();
}) */