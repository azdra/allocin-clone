import {getElement, substr, refreshSearch} from './lib.js';
import cfg from '../../config.js';

let search_moovie = document.getElementById('search-moovie');
search_moovie.value = '';
let ipu = 'movie';

let inputsearch = document.getElementById('input-search');
inputsearch.addEventListener('click', () => {
	fetchResult(ipu, search_moovie);
});

let result_pnl = document.querySelector('#result');
let mediaMomentDay = document.getElementById('mediaMomentDay');
let mediaMomentWeek = document.getElementById('mediaMomentWeek');
document.getElementById('movie').click();

let page = 1;
let tbl_genre=[{id:28,name:'Action'},{id:12,name:'Adventure'},{id:16,name:'Animation'},{id:35,name:'Comedy'},{id:80,name:'Crime'},{id:99,name:'Documentary'},{id:18,name:'Drama'},{id:10751,name:'Family'},{id:14,name:'Fantasy'},{id:36,name:'History'},{id:27,name:'Horror'},{id:10402,name:'Music'},{id:9648,name:'Mystery'},{id:10749,name:'Romance'},{id:878,name:'Science Fiction'},{id:10770,name:'TV Movie'},{id:53,name:'Thriller'},{id:10752,name:'War'},{id:37,name:'Western'}];

search_moovie.addEventListener('click', e => {

	refreshSearch(result_pnl);

	let _val = e.target.value;
	if(_val.length <= 0) return;
	fetchMovie(`https://api.themoviedb.org/3/search/${ipu}?api_key=${cfg.apikey}&language=${cfg.lang}&page=${page}&include_adult=true&query=${_val}`);

	result_pnl.classList.remove('result-collaps');
} );
search_moovie.addEventListener('keyup', e => updateInput(search_moovie, e) );
const updateInput = (input, e) => {
	
	refreshSearch(result_pnl);
	
	let _val = input.value;
	fetchMovie(`https://api.themoviedb.org/3/search/${ipu}?api_key=${cfg.apikey}&language=${cfg.lang}&page=${page}&include_adult=true&query=${_val}`);

	if(input.value.length == 0){
		result_pnl.classList.add('result-collaps');
	} else {
		result_pnl.classList.remove('result-collaps');
	}

	let _key = {fetch:[13], close: [27]};
	/*eslint no-unused-vars: "off"*/ 
	for(let k in _key){
		if(e.keyCode == _key.close) return result_pnl.classList.add('result-collaps');
		if(e.keyCode == _key.fetch) {
			fetchResult(ipu, _val);
		}
	}
};

let tbg_media = {
	movieid: [
		'Rechercher un film...',
		'Les films du moment:',
		'Les films du Week-end:'
	],
	tvid: [
		'Rechercher une series...',
		'Les series du moment:',
		'Les series du Week-end:'
	],
	personid: [
		'Rechercher un acteur...',
		'Les acteurs du jour:',
		'Les acteurs du Week-end:'
	],
	allid: [
		'Rechercher un film, une serie, un acteur...',
		'Les tendances du jour:',
		'Les tendances du Week-end:'
	],
};

document.addEventListener('click', e => {

	if(e.target.id == 'search-moovie') return;

	if(!e.target.classList.contains('result-item-container')){
		result_pnl.classList.add('result-collaps');
	}

	if(e.target.getAttribute('data-movieid')){
		return fetchMovieId(e.target.getAttribute('data-movieid'));
	}

	if(e.target.getAttribute('data-tvid')){
		return fetchTvId(e.target.getAttribute('data-tvid'));
	}
	
	if(e.target.getAttribute('data-personid')){
		return fetchPersoneId(e.target.getAttribute('data-personid'));
	}
 
	if(e.target.getAttribute('data-type')){
		if(ipu == e.target.getAttribute('data-type')) return;
		// if(e.target.getAttribute('data-type') != 'movie') return;
		let currentmovie = document.querySelector('.current-movie');
		let currentmovieweek = document.querySelector('.current-week-movie');
		refreshSearch(currentmovie, currentmovieweek, result_pnl);
		// refreshSearch(currentmovieweek);
		// refreshSearch(result_pnl);

		ipu = e.target.getAttribute('data-type');
		let ipuid = ipu+'id';

		search_moovie.setAttribute('placeholder', tbg_media[ipuid][0]);
		mediaMomentDay.innerHTML = tbg_media[ipuid][1];
		mediaMomentWeek.innerHTML = tbg_media[ipuid][2];

		elementDay(`https://api.themoviedb.org/3/trending/${ipu}/day?api_key=${cfg.apikey}`, ipuid);
		elementWeek(`https://api.themoviedb.org/3/trending/${ipu}/week?api_key=${cfg.apikey}`, ipuid);
	}
});

search_moovie.addEventListener('search', e => {
	if(e.target.value >= 0){
		refreshSearch(result_pnl);
		result_pnl.classList.add('result-collaps');
	}
});

const fetchMovie = async (url) => {
	/*eslint no-undef: "off"*/
	try {
		let pro = new Promise((r) => {
			fetch(url)
				.then(data => {
					if(data.ok === true) {
						return r(data.json().then(res => {
							// let result_pnl = document.querySelector('#result');
							if(res.total_results == 0) return resultsNotify(result_pnl, 'Il n\'y a aucun élément correspondant à votre requête.');
							MovieResults(result_pnl, res);

							let search_info = document.createElement('p');
							search_info.innerHTML = `Tous les résultats pour "<span class='search-value'>${search_moovie.value}</span>"`;
							search_info.classList.add('search-info');
							search_info.setAttribute('id', 'allresult');
							result_pnl.append(search_info);
						}));
					} else {
					// return console.log("z");
					}
				});
		});
		pro.then(() => {
			console.log('fff');
		});
	} catch (error) {
		// console.log("re")
	}
};

const resultsNotify = (pnl, msg) => {
	refreshSearch(result_pnl);
	let p = document.createElement('p');
	p.classList.add('search-info');
	p.innerHTML = msg;
	pnl.append(p);
};

const MovieResults = (pnl, res) => {
	refreshSearch(result_pnl);

	const max_preview = 5;

	let data = res.results.slice(0, max_preview);
	/* if(res.results.length >= max_preview){
		data = res.results.slice(0, max_preview);
	} else {
		data = res.results;
	} */

	for (let i = 0; i < data.length; ++i) {

		let _p = getElement(res.results[i]);

		let base = document.createElement('div');
		base.classList.add('result-item-container');

		let panel = document.createElement('div');
		panel.classList.add('result-item-content');

		let img = document.createElement('img');
		img.classList.add('img-bg');
		img.setAttribute('src', _p.poster_path ? `https://image.tmdb.org/t/p/w600_and_h900_bestv2/${_p.poster_path}` : './assets/img/unknown.png' ); //`https://image.tmdb.org/t/p/w600_and_h900_bestv2/${data.poster_path}`
		img.setAttribute('alt', `Poster du film: ${_p.title}`);

		let bg_elm = document.createElement('div');
		bg_elm.classList.add('movie-info-bg');

		let title = document.createElement('p');
		title.classList.add('movie-info-title');
		title.innerHTML = _p.title;

		let lt = document.createElement('p');
		lt.classList.add('movie-info-subtitle');
		let gen = '';
		for(let k in _p.genres){
			for(let e in tbl_genre){
				if(tbl_genre[e].id == _p.genres[k]){
					gen += `${tbl_genre[e].name}, `;
				}
			}
		}
		lt.innerHTML = (gen == '' || !gen) ? 'Aucun genre donné' : gen.slice(0, gen.length-2);

		bg_elm.append(title, lt);

		let btn = document.createElement('div');
		btn.classList.add('btn-click');
		btn.dataset.movieid = _p.id;

		panel.append(img, bg_elm, btn);
		base.append(panel);
		pnl.append(base);
	}
};

const elementDay = (url, id) => {
	fetch(url)
		.then(j => {
			if(j.ok){
				return j.json().then(res => {

					let parent = document.querySelector('.current-movie');
					for (let i = 0; i < res.results.length; i++) {
						let base = document.createElement('div');
						let p = getElement(res.results[i]);
					
						base.classList.add('current-movie-elm');

						let img = document.createElement('img');
						img.classList.add('img-current-moment');
						img.setAttribute('src', p.poster_path ? `https://image.tmdb.org/t/p/w600_and_h900_bestv2/${p.poster_path}` : './assets/img/unknown.png');

						let date = document.createElement('span');
						date.classList.add('current-movie-date');
						date.innerHTML = p.release_date ? `${p.release_date.split('-')[2]}/${p.release_date.split('-')[1]}/${p.release_date.split('-')[0]}` : '';

						let text = document.createElement('span');
						text.classList.add('current-movie-title');
						text.innerHTML = substr(p.title, 22);

						let hover = document.createElement('div');
						hover.classList.add('current-movie-collaps');
						hover.classList.add('current-movie-mouse');
						hover.dataset[id] = p.id;

						base.append(img, text, date, hover);
						parent.append(base);	
					}
				});
			}
		});
};
const elementWeek = (url, id) => {
	fetch(url)
		.then(j => {
			if(j.ok){
				return j.json().then(res => {
					let parent = document.querySelector('.current-week-movie');
					for (let i = 0; i < res.results.length; i++) {
						let p = getElement(res.results[i]);

						let base = document.createElement('div');
						base.classList.add('current-week-movie-elm');

						let img = document.createElement('img');
						img.classList.add('img-current-week-moment');
						img.setAttribute('src', p.poster_path ? `https://image.tmdb.org/t/p/w600_and_h900_bestv2/${p.poster_path}` : './assets/img/unknown.png');

						let date = document.createElement('span');
						date.classList.add('current-movie-date');
						date.innerHTML =  p.release_date ? `${p.release_date.split('-')[2]}/${p.release_date.split('-')[1]}/${p.release_date.split('-')[0]}` : '';

						let text = document.createElement('span');
						text.classList.add('current-movie-title');
						text.innerHTML = substr(p.title, 24);

						let hover = document.createElement('div');
						hover.classList.add('current-movie-collaps');
						hover.classList.add('current-movie-mouse');
						hover.dataset[id] = p.id;

						base.append(img, text, date, hover);
						parent.append(base);	
					}
				});
			}
		});
};

const fetchResult = (media, val) => {
	// let url = window.location.protocol + '//' + window.location.host + window.location.pathname + 'search/movie/index.html';

	let url;
	if(window.location.pathname.includes('.html')){
		url = `${window.location.origin}/search/`;
	} else {
		url = `${window.location.toString()}search/`;
	}

	let uuurl = new URL(`${url}`);
	uuurl.searchParams.set('media', media);
	uuurl.searchParams.set('query', val);
	window.location.assign(uuurl); 
};

const fetchMovieId = (id) => {
	//let url = window.location.protocol + '//' + window.location.host + window.location.pathname + 'movie/index.html';
	//console.log(url)
	//console.log(window.location)

	let url;
	if(window.location.hostname == '127.0.0.1') {
		if(window.location.pathname.includes('.html')){
			url = `${window.location.origin}/movie/index.html`;
		} else {
			url = `${window.location.toString()}movie/index.html`;
		}
	} else {
		if(window.location.pathname.includes('.html')){
			url = `${window.location.origin}/${window.location.pathname.split('/')[1]}/movie/index.html`;
		} else {
			url = `${window.location.toString()}movie/index.html`;
		}
	}

	console.log(window.location);
	let uuurl = new URL(`${url}`);
	uuurl.searchParams.set('movieid', id);
	window.location.assign(uuurl);
};

const fetchTvId = (id) => {
	//let url = window.location.protocol + '//' + window.location.host + window.location.pathname + 'tv-show/index.html';

	let url;
	if(window.location.hostname == '127.0.0.1') {
		if(window.location.pathname.includes('.html')){
			url = `${window.location.origin}/tv-show/index.html`;
		} else {
			url = `${window.location.toString()}tv-show/index.html`;
		}
	} else {
		if(window.location.pathname.includes('.html')){
			url = `${window.location.origin}/${window.location.pathname.split('/')[1]}index.html`;
		} else {
			url = `${window.location.toString()}tv-show/index.html`;
		}
	}

	console.log(url);

	let uuurl = new URL(`${url}`);
	// console.log(uuurl)
	uuurl.searchParams.set('tv-show', id);
	window.location.assign(uuurl);
};

const fetchPersoneId = (id) => {
	// let url = window.location.protocol + '//' + window.location.host + window.location.pathname + 'person/index.html';

	let url;
	if(window.location.hostname == '127.0.0.1') {
		if(window.location.pathname.includes('.html')){
			url = `${window.location.origin}/person/index.html`;
		} else {
			url = `${window.location.toString()}person/index.html`;
		}
	} else {
		if(window.location.pathname.includes('.html')){
			url = `${window.location.origin}/${window.location.pathname.split('/')[1]}index.html`;
		} else {
			url = `${window.location.toString()}person/index.html`;
		}
	}

	let uuurl = new URL(`${url}`);
	// console.log(uuurl)
	uuurl.searchParams.set('person', id);
	window.location.assign(uuurl);
};

search_moovie.setAttribute('placeholder', 'Rechercher un film');
mediaMomentDay.innerHTML = 'Les films du moment:';
mediaMomentWeek.innerHTML = 'Les films du Week-end:';
elementDay(`https://api.themoviedb.org/3/trending/movie/day?api_key=${cfg.apikey}`, 'movieid');
elementWeek(`https://api.themoviedb.org/3/trending/movie/week?api_key=${cfg.apikey}`, 'movieid');

