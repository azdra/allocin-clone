import {getElement, substr, refreshSearch} from './lib.js';
import cfg from '../../../config.js';

let params = new URL(document.location);

let media = params.searchParams.get('media');
let query = params.searchParams.get('query');
let page = 1;

window.onload = () => {
   // fetchResult()
}

const fetchResult = () => {
    refreshSearch(document.body)

    fetch(`https://api.themoviedb.org/3/search/${media}?api_key=${cfg.apikey}&language=${cfg.lang}&query=${query}&page=${page}&include_adult=true`)
        .then(j => j.json())
        .then(res => result(res))
}

const result = (res) => {
    let info = document.createElement('p');
    info.innerHTML = page;

    let back = document.createElement('button');
    back.innerHTML = '<<';
    back.setAttribute('id', 'paginationback');

    let front = document.createElement('button');
    front.innerHTML = '>>';
    front.setAttribute('id', 'paginationfront');

    document.body.append(info, back, front);

    addEvent(res)
}

document.title = `${query} ~ AlluCiné`;

const addEvent = (res) => {
    
    let btnback = document.getElementById('paginationback');
    let btnfront = document.getElementById('paginationfront');

    btnback.addEventListener('click', () => {
        if(page == 1) return
        page--;
        fetchResult()
    });

    btnfront.addEventListener('click', () => {
        if(page == res.total_pages) return;
        page ++;
        fetchResult()
    });
}