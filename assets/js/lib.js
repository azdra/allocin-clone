/**
 * Fonction qui transforme les minutes d'un film en heure
 * @param {Number} n Duré du film en minute
 * @returns {Array.<{hours: String, minutes: String}>}
 */
const mtoh = (n) => {
	let num = n;
	let hours = (num / 60);
	let rhours = Math.floor(hours);
	let minutes = (hours - rhours) * 60;
	let rminutes = Math.round(minutes);
	return {
		hours: rhours,
		minutes: rminutes
	};
};

/**
 * @param {Array} data resultat du fetch
 * @returns {Array.<{adult: Boolean, backdrop_path: String, budget: Number, genres: (Array|String|null|Number), homepage: String, id: Number, original_language: String, title: string, overview: String, poster_path: String, popularity: Number}>}
 */
const getElement = (data) => {
	let p = {
		/**
		 * @adult
		 * return true or false
		 * movie
		 */
		adult: data.adult,

		/**
		 * @backdrop_path
		 * return img background path of the element
		 * movie, series
		 */
		backdrop_path: data.backdrop_path,

		/**
		 * @budget
		 * return the movie budget
		 * movie
		 */
		budget: data.budget,

		/**
		 * @genres
		 * return the genre id ou genre name
		 * @dataGenre return the genre name and genre id
		 * @dataGenre_ids return the genre id
		 * movie, series
		 */
		genres: data.genres ?? data.genre_ids,

		/**
		 * @homepage
		 * return the element link
		 * series
		 */
		homepage: data.homepage,

		/**
		 * @id
		 * return elem id
		 * movie, series
		 */
		id: data.id,
		
		/**
		 * @original_language
		 * return element language
		 * series
		 */
		original_language: data.original_language,

		/**
		 * @title
		 * return the title element
		 * movie, serie, person
		 */
		title: data.title ?? data.original_title ?? data.name,

		/**
		 * @overview
		 * return elemen description
		 * movie, serie
		 */
		overview: data.overview,

		/**
		 * @poster_path
		 * return img poster path of the element
		 * @dataPoster_path for movie and serie
		 * @dataProfile_path for person
		 */
		poster_path: data.poster_path ?? data.profile_path,

		/**
		 * @popularity
		 * return popularity of the element
		 * movie, serie
		 */
		popularity: data.popularity,

		/**
		 * @release_date
		 * return 
		 */
		release_date: data.release_date ?? data.first_air_date ?? '',

		episode_time: data.episode_run_time,

		networks: data.networks,

		biography: data.biography,

		deathday: data.deathday,

		birthday: data.birthday,
		
		production_companies: data.production_companies,
		production_countries: data.production_countries,
		revenue: data.revenue,
		runtime: data.runtime,
		spoken_languages: data.spoken_languages,
		status: data.status,
		video: data.video,
		

		tagline: data.tagline,
		vote_average: data.vote_average,
		vote_count: data.vote_count,
	};
	return p;
};

/**
 * @param {String} str Une phrase | mots etc
 * @param {Number} max Nombre de caractère maximum
 * @returns {String} return la phrase modifié ou pas
 */
const substr = (str, max) => {

	if(str.length >= max){
		return str.substring(0, max-3)+'...';
	} else {
		return str;
	} 
	
};

const refreshSearch = (...arg) => {
	for (let i = 0; i < arg.length; i++) {
		for (let k = arg[i].children.length-1; k >=0; --k) {
			arg[i].removeChild(arg[i].children[k]);
		}
	}
};

export {mtoh, getElement, substr, refreshSearch};