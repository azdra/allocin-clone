# AlloCinéClone

The objective is to create a website to search for information on movies. Thanks to this site, we will be able to search for any film and know all the information related to the film (title, duration, director, etc..).

## Technologies used:
<ul>
    <li>HTML</li>
    <li>FrameWork CSS: Foundation</li>
    <li>SCSS</li>
    <li>JavaScript</li>
    <li>ESLint</li>
</ul>

### Installation
```
1 > git clone https://gitlab.com/baptiste.b/allocin-clone.git
2 > cd allocin-clone
3 > npm i
4 > touch config.js
```

> In the file <code>config.js</code>
```js
export default {
    apikey: '',
    // token: '',
    lang: 'en-US'
}
```
Go to <a href ="https://developers.themoviedb.org/3/getting-started/introduction">The Movie Database</a> for you to create your `apikey`.
<p>Get the key and put it in <code>'apikey'</code></p>

## Preview
![img.png](img.png)
![img_1.png](img_1.png)
